from setuptools import setup, find_packages

setup(name='CoverAPI',
      version='0.1',
      description='Cover API library',
      author='Jelmer van der Linde',
      author_email='jelmer@ikhoefgeen.nl',
      packages=find_packages(),
      install_requires=[],
      test_suite="tests",
)