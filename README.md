# Cover API
This API should allow you to access the various Cover API's, such as the website for login and committee information, Incass-O-matic for the creation of direct debits, and KAST for the purchase of stuff and fetching of credit balances.

Currently the only goal is to implement the most basic stuff needed for svcover.nl-global sessions.