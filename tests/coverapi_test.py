from __future__ import absolute_import
from . import context
import unittest
from coverapi import CoverAPI

class LoginTest(unittest.TestCase):
    def test_login(self):
        api = CoverAPI('http://www.svcover.dev/api.php', 'coverapi', 'testerdetest')
        session = api.login('jelmer@ikhoefgeen.nl', 'test')
        self.assertEqual(session.user['email'], 'jelmer@ikhoefgeen.nl')
