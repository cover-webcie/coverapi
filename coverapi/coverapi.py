from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals
from .utils import urlencode, signed_request
import json

class APIError(Exception):
	pass

class CoverAPI(object):
	def __init__(self, url, app, secret, timeout=10):
		self.url = url
		self.app = app
		self.secret = secret
		self.timeout = timeout

	def request(self, params, data=None):
		return signed_request(
			app=self.app,
			secret=self.secret,
			url='%s?%s' % (self.url, urlencode(params)),
			post=data,
			timeout=self.timeout)

	def request_json(self, params, data=None):
		response = self.request(params, data).read().decode('utf-8')
		response_data = json.loads(response)
		if 'error' in response_data and response_data['error'] is not None:
			raise APIError("Cover API Error: {}".format(response_data['error']))
		return response_data

	def get_session(self, session_id):
		response = self.request_json({
			'method': 'session_get_member',
			'session_id': session_id
		})
		return CoverSession(self, session_id, response['result'])

	def login(self, email, password):
		response = self.request_json(
			{'method': 'session_create'},
			{'email': email, 'password': password, 'application': self.app})
		session_id = response['result']['session_id']
		return self.get_session(session_id)


class CoverSession(object):
	def __init__(self, api, session_id, user_data):
		self.api = api
		self.session_id = session_id
		self.user = user_data

	
