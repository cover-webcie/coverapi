import hashlib

# urlopen for py2 and py3
try:
	from urllib2 import Request, urlopen
except:
	from urllib.request import Request, urlopen

# urlencode with utf8 support
try:
	from urllib.parse import urlencode
except:
	from urllib import urlencode as _native_urlencode
	def urlencode(data):
		'''Version of urlencode that encodes utf8 strings when possible'''
		def _encode(value):
			try:
				return value.encode('utf8')
			except AttributeError:
				return value

		return _native_urlencode({field: _encode(value) for field, value in data.items()})


def signed_request(app, secret, url, post=None, timeout=30):
	body = urlencode(post).encode('utf8') if post is not None else None
	checksum = hashlib.sha1((body if body is not None else b'') + secret.encode('utf8')).hexdigest()
	request = Request(
		url=url,
		data=body,
		headers={
			'X-App': app,
			'X-Hash': checksum
		})
	return urlopen(request, timeout=timeout)


def http_digest(body):
	checksum = hashlib.sha256(body).hexdigest()
	return 'SHA-256=%s' % checksum